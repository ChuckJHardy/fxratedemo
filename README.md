# FXRate Demo Application

Demo Application for the FXRate Service.

![Example](./Example.gif)

### Installation

Install dependencies:

[Direnv](https://github.com/zimbatm/direnv) or alternative for managing environment variables.

    $ brew install direnv

Copy example environment variables file:

    $ cp .envrc.example .envrc

Execute:

    $ bin/setup

### Usage

Start development server:

    $ bin/server

Open the [site](http://localhost:9298):

    $ open http://localhost:9298

### Testing

Run Unit Tests:

    $ bin/rspec

### Todo

Future Tasks

- [ ] Reset Button
- [ ] Error Handling
- [ ] i18n
