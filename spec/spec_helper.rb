require "spec_helper_lite"

Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

require "sinatra"
require "rack/test"

set :environment, :test
set :run, false
set :raise_errors, true
set :logging, true

def app
  FXRateDemo::Application
end

RSpec.configure do |config|
  config.include Rack::Test::Methods
end
