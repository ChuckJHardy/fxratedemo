RSpec.describe FXRateDemo::AmountPresenter, type: :presenter do
  subject(:presenter) do
    described_class.present(amount: 13.249138, currency: "GBP")
  end

  it "returns expected string" do
    expect(presenter).to eq("13.25 GBP")
  end
end
