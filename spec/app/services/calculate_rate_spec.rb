RSpec.describe FXRateDemo::CalculateRate, type: :service do
  subject(:service) { described_class.call(amount: "11.54", rate: "0.871") }

  it "returns rate as a float" do
    expect(service).to be_a(Float)
  end

  it "returns expected rate" do
    expect(service).to eq(13.24913892078071)
  end
end
