Feature: HomePage

  Scenario: Loads
    Given I visit "/"
    Then I should see "Currency Converter" within ".container"

  Scenario: Exchanged Value
    Given I visit "/"
    When I select "2015-05-14" from "date" within ".container"
    And I select "GBP" from "base" within ".container"
    And I select "USD" from "counter" within ".container"
    And I fill in "amount" with "11" within ".container"
    And I press "Convert" within ".container"
    Then I should see "17.37 USD" within ".amount"

  Scenario: Prefil Values
    Given I visit "/?date=2015-05-14&amount=11&base=GBP&counter=USD"
    Then I should see "17.37 USD" within ".amount"
