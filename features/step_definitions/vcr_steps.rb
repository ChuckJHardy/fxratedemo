Given /^I visit "([^"]*)"$/ do |url|
  VCR.use_cassette("features/cassettes/requests") do
    visit url
  end
end
