require "vcr"

VCR.configure do |c|
  c.hook_into :webmock
  c.cassette_library_dir     = "features/cassettes"
  c.default_cassette_options = { record: :new_episodes }
end
