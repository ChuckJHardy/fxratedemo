module FXRateDemo
  class AmountPresenter
    def initialize(amount:, currency:)
      @amount = amount
      @currency = currency
    end

    def self.present(*args)
      new(*args).present
    end

    def present
      "#{amount} #{@currency}"
    end

    private

    def amount
      sprintf("%.2f", @amount)
    end
  end
end
