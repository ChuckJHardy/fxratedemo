require "./app/services/calculate_rate"
require "./app/presenters/amount_presenter"

module FXRateDemo
  class SearchController
    attr_reader :params

    def initialize(app:, params:)
      @app = app
      @params = params
    end

    def self.index(*args)
      new(*args).index
    end

    def index
      @app.erb :index, layout: :application, locals: locals
    end

    private

    def locals
      { answer: answer, dates: dates, currencies: currencies }
    end

    def calculated_amount
      FXRateDemo::CalculateRate.call(amount: amount, rate: search.fetch("rate"))
    end

    def answer
      if amount
        FXRateDemo::AmountPresenter.present(
          amount: calculated_amount,
          currency: params["counter"]
        )
      end
    end

    def search
      ExchangeRate.at(params["date"], params["base"], params["counter"])
    end

    def amount
      params["amount"]
    end

    def dates
      wrapper.dates.fetch("dates", [])
    end

    def currencies
      wrapper.currencies.fetch("currencies", []).sort
    end

    def wrapper
      @wrapper = ExchangeRate.new
    end
  end
end
