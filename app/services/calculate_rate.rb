module FXRateDemo
  class CalculateRate
    def initialize(amount:, rate:)
      @amount = amount
      @rate = rate
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      amount / rate
    end

    private

    def amount
      @amount.to_f
    end

    def rate
      @rate.to_f
    end
  end
end
