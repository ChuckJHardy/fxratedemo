ENV["RACK_ENV"] ||= "development"

require "./application"

run FXRateDemo::Application
