require "sinatra"
require "sinatra/base"
require "sinatra/reloader" if development?
require "exchange_rate"

require "./app/controllers/search_controller"

module FXRateDemo
  class Application < Sinatra::Base
    # :nocov:
    set :views, Proc.new { File.join(root, "app/views") }

    configure :development do
      register Sinatra::Reloader
    end

    ExchangeRate.configure do |config|
      config.domain = ENV["FX_RATE_URL"]
      config.provider = :european_central_bank
    end
    # :nocov:

    get "/" do
      redirect("/search")
    end

    get "/search" do
      FXRateDemo::SearchController.index(app: self, params: params)
    end
  end
end
